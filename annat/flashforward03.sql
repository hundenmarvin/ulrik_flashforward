-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Värd: 127.0.0.1
-- Tid vid skapande: 03 mars 2015 kl 21:30
-- Serverversion: 5.6.17
-- PHP-version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databas: `flashforward`
--

-- --------------------------------------------------------

--
-- Tabellstruktur `replies`
--

CREATE TABLE IF NOT EXISTS `replies` (
  `reply_id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `topic_id` int(8) unsigned NOT NULL,
  `user_id` int(8) unsigned NOT NULL,
  `comment` text NOT NULL,
  `date_posted` date NOT NULL,
  PRIMARY KEY (`reply_id`),
  KEY `topic_id` (`topic_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellstruktur `swedrftghy`
--

CREATE TABLE IF NOT EXISTS `swedrftghy` (
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `wert` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumpning av Data i tabell `swedrftghy`
--

INSERT INTO `swedrftghy` (`date`, `wert`) VALUES
('2015-03-02 11:38:47', 234);

-- --------------------------------------------------------

--
-- Tabellstruktur `topics`
--

CREATE TABLE IF NOT EXISTS `topics` (
  `topic_id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(8) unsigned NOT NULL,
  `title` varchar(128) NOT NULL,
  `url` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `date_posted` date NOT NULL,
  PRIMARY KEY (`topic_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumpning av Data i tabell `topics`
--

INSERT INTO `topics` (`topic_id`, `user_id`, `title`, `url`, `content`, `date_posted`) VALUES
(1, 9, 'Den kommande singulariteten', 'http://waitbutwhy.com/2015/01/artificial-intelligence-revolution-1.html', 'En blog-post som fÃ¶rklarar lÃ¤ttfÃ¶rstÃ¥eligt i detalj om den teknologiska singulariteten.', '2015-02-28'),
(2, 8, 'Microsoft lanserar Augmented Reality', 'https://www.youtube.com/watch?v=aThCr0PsyuA', 'Microsoft ger sig in i VR-kriget.', '2015-02-28'),
(3, 8, 'Lever vi i en simulering?', 'http://www.simulation-argument.com/simulation.html', 'En akademisk rapport av Nick BostrÃ¶m', '2015-02-28');

-- --------------------------------------------------------

--
-- Tabellstruktur `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(16) NOT NULL,
  `password` varchar(64) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumpning av Data i tabell `users`
--

INSERT INTO `users` (`user_id`, `username`, `password`) VALUES
(5, 't1000', 't'),
(6, 'luke', 'l'),
(7, 'glados', 'g'),
(8, 'gizmo', 'g'),
(9, 'ed209', 'e'),
(10, 'wall-e', 'w'),
(11, 'r2d2', 'r'),
(12, 'c3po', 'c'),
(13, 'sonny', 's'),
(14, 'mr_smith', 'm');

-- --------------------------------------------------------

--
-- Tabellstruktur `votes`
--

CREATE TABLE IF NOT EXISTS `votes` (
  `vote_id` int(16) unsigned NOT NULL AUTO_INCREMENT,
  `topic_id` int(8) unsigned NOT NULL,
  `vote` int(8) NOT NULL,
  `user_id` int(8) unsigned NOT NULL,
  `date_voted` date NOT NULL,
  PRIMARY KEY (`vote_id`),
  KEY `topic_id` (`topic_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Restriktioner för dumpade tabeller
--

--
-- Restriktioner för tabell `replies`
--
ALTER TABLE `replies`
  ADD CONSTRAINT `replies_ibfk_1` FOREIGN KEY (`topic_id`) REFERENCES `topics` (`topic_id`);

--
-- Restriktioner för tabell `votes`
--
ALTER TABLE `votes`
  ADD CONSTRAINT `votes_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`),
  ADD CONSTRAINT `votes_ibfk_1` FOREIGN KEY (`topic_id`) REFERENCES `topics` (`topic_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
